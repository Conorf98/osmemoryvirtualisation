#ifndef CONSTANTSH
#define CONSTANTSH

//Address space = 16bits -> 65536 bytes  (4 hex)
const int ADDRESS_SPACE_SIZE = 65536;

//page size = 256 bytes
//page entry = 2 bytes
//therefore page table size is 512 bytes
const int PAGE_SIZE = 256;
const int PAGE_TABLE_SIZE = 512;

//65536 divided by 256 pages = 256 frames
const int FRAME_COUNT = 256;

//Random amount of memory to be set between 2048 and 20480
const int MAX_MEMORY_VALUE = 20480;
const int MIN_MEMORY_VALUE = 2048;

//will need random character when setting memory
//Ascii characters are between 33 and 126
const int MAX_ASCII_VALUE = 126;
const int MIN_ASCII_VALUE = 33;

//there are 256 frames
//first 2 are dedicated to page table
const int MAX_FRAME_VALUE = 256;
const int MIN_FRAME_VALUE = 2;
#endif