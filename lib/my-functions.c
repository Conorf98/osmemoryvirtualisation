#include <stdio.h>
#include <stdlib.h>
#include "my-functions.h"
//#include "constants.h"

//modified code at https://www.geeksforgeeks.org/generating-random-number-range-c/
int getRandomNumberInRange(int min, int max)
{     
	int num = (rand() % (max - min + 1)) + min;  
	return num;
}


void writeMemoryToAddrSpace(char* addressSpace, int randomMemory, int startingIndex)
{
	int endIndex = startingIndex + randomMemory;

	for(int i = 0; i < 65536; ++i)
	{
		if( i < 512)
		{
			addressSpace[i] = 0;
		}
		else if(i >= startingIndex && i <= endIndex)
		{
			addressSpace[i] = getRandomNumberInRange(33, 125);
		}
		else
		{
			addressSpace[i] = '~';
		}
	}
	
}