#ifndef MY_FUNCTIONS
#define MY_FUNCTIONS

  int getRandomNumberInRange(int min, int max);
  void writeMemoryToAddrSpace(char* addressSpace, int randomMemory, int startingIndex);
#endif