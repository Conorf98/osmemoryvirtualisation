
#include "../lib/my-functions.h"
#include "../lib/constants.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>
int main()
{
	srand(time(0)); 
	unsigned char *addressSpace = malloc(ADDRESS_SPACE_SIZE);
	char *pageTable = malloc(PAGE_TABLE_SIZE);
	int randomAmountOfMemory = getRandomNumberInRange(MIN_MEMORY_VALUE, MAX_MEMORY_VALUE);
	//initialisation
	int randomMemoryAddress = 0;
	int startingFrame = 0;
	//get a random index in for the memory to start at and make sure that it will not exceed the address space size
	do
	{
		startingFrame = getRandomNumberInRange(MIN_FRAME_VALUE, MAX_FRAME_VALUE);
        randomMemoryAddress = startingFrame * 256; 
	}while(randomMemoryAddress + randomAmountOfMemory > ADDRESS_SPACE_SIZE);

	
	writeMemoryToAddrSpace(addressSpace,randomAmountOfMemory,randomMemoryAddress);
	
	//found way to format print out for a table here -> https://stackoverflow.com/questions/8548909/smart-way-to-format-tables-on-stdout-in-c
	printf("%6s %6s\n", "Amount of Memory", "Starting Address");
	printf("%6i %6i %6i\n", randomAmountOfMemory, randomMemoryAddress, startingFrame);

	  FILE *file = fopen("./physical_memory.txt", "w");

  if (file == NULL) {
    printf("Error opening file to write. Gave up.\n");
    return 4;
  }
  
  //get the frame the memory starts on

  int currFrame = 0;
int endAddress = randomMemoryAddress + randomAmountOfMemory;
  //find how many frames are occupied
  for(int i = randomMemoryAddress; i < endAddress; ++i)
  {
	  if(i % PAGE_SIZE == 0)
	  {
		  
		  currFrame++;
	  }
  }
  
  //link the frames to the page table
  for(int i = 0; i < currFrame; ++i) {
printf("%6i\n",startingFrame);
	addressSpace[i] = (char)startingFrame;
	startingFrame++;
  }
  
  currFrame = 0;
   fprintf(file, "%-8s  | %-8s\n", "Address", "Frame");
  // Write in format: 1,2,3,4,5,
  for(int i = 0; i < ADDRESS_SPACE_SIZE; ++i) {

  
  if(i % 256 == 0)
  {
	  currFrame++;
	  fprintf(file,"===========================FRAME %i===========================\n",currFrame);
  }
  
  if(i<512)
  {
	  fprintf(file,"0x%-8i  | %8i\n", i, addressSpace[i]);
  }
  else if (i == 512)
  {
	  fprintf(file, "%-8s  | %-8s| %-8s\n", "Address", "Frame", "Content");
  }
  else
  {
	fprintf(file,"0x%-8i  | %-8i| %-8c\n", i, currFrame,(char) addressSpace[i]);
  }
  }
  
  //printing page table
  /*fprintf(file, "\n\n%-8s | %-8s\n", "Entry", "Frame");
  for(int i = 0; i < PAGE_TABLE_SIZE; ++i) {

	fprintf(file,"%-8i | %-8i\n", i, pageTable[i]);
  }
*/
  printf("Done.\n");

  // Don't forget to free up resources when you're don with them
  fclose(file);
  

  // Getting Hex input
  unsigned char input_address;
  printf("Enter an Address here > ");
  scanf("%hhX", &input_address);
  
  
  //code taken from moodle from bit shifting example
  unsigned char offset_mask = 0x00FF;
  unsigned char offset = input_address & offset_mask;
  unsigned char vpn = input_address >> 8;
  printf("Address 0x%X = VPN: 0x%X, offset: 0x%X\n", input_address, vpn, offset);

  //add the offset and address then send them to that index in the addressSpace
  
  free(addressSpace);
  free(pageTable);
	return 0;
}



//code taken from moodle on how to get max value -> shows how to scanf and to malloc
// Allocate enough memory for **count** of integers
//  int *numbers = malloc(count * sizeof(int));

//  for(int i = 0; i < count; ++i) {

//    printf("Please enter number %d > ", (i + 1));
//    scanf("%d", &numbers[i]);

//    if (max == NULL || numbers[i] > *max) {
//      max = &numbers[i];
//    }
//  }


//printf("----------------------------\n");
//  printf("The size of a CHAR is %lu bytes\n", sizeof(char));
//  printf("The size of a BOOL (stdbool) is %lu bytes\n", sizeof(bool));


 // code taken from Malloc example on moodle -> to be used for writing to a file
  // Write the final form of the collection to file
//  printf("Writing collection to disk...");

//  FILE *file = fopen("./realloc.txt", "w");

//  if (file == NULL) {
//    printf("Error opening file to write. Gave up.\n");
//    return 4;
//  }

  // Write in format: 1,2,3,4,5,
//  for(arr_size i = 0; i < next_index; ++i) {
//    fprintf(file, "%d,", array[i]);
//  }

//  printf("Done.\n");

  // Don't forget to free up resources when you're don with them
//  fclose(file);
//  free(array);